﻿using Xamarin.Forms;

namespace TestWebApp
{
	public partial class WebViewPage : ContentPage
	{
		public WebViewPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		void Navigate_Clicked(object sender, System.EventArgs e)
		{
			webView.Source = new UrlWebViewSource { Url = browse.Text };
		}

		void Menu_Clicked(object sender, System.EventArgs e)
		{
			// Using script from page
			var _Script = string.Format("ToggleMainNavigation.toggle()");
			webView.Eval(_Script);

			//Add some animation(just for fun)
			if(bttnMenu.Rotation > 0) bttnMenu.Rotation = -180;
			else bttnMenu.Rotation = 0;

			var animation = new Animation(callback: d => bttnMenu.Rotation = d, start: bttnMenu.Rotation, end: bttnMenu.Rotation + 180, easing: Easing.SpringOut);
			animation.Commit(bttnMenu, "Loop", length: 400);
		}


	}
}
